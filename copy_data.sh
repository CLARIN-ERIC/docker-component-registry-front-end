#!/bin/bash
source "${DATA_ENV_FILE:-copy_data.env.sh}"
echo "Using properties from ${DATA_ENV_FILE}"

DISTR_FILE="distr_$(date +%Y%m%d%H%M%S).tar.gz"
TARGET_DIR="sources"

init_data () {
    echo "Retrieving and unpacking application from ${SRC_DISTRIBUTION}"
    
	if ! wget -q -O "${DISTR_FILE}" "${SRC_DISTRIBUTION}"; then
		if [ -e "${DISTR_FILE}" ]; then 
			rm "${DISTR_FILE}" 
		fi
		echo "Failed to retrieve file from ${SRC_DISTRIBUTION}!"
		exit 1
	fi
    
    mkdir -p "${TARGET_DIR}" && \
	    ( cd "${TARGET_DIR}" && tar -z -x -f ../${DISTR_FILE} --strip-components 1 > /dev/null ) && \
    	rm ${DISTR_FILE}
}

cleanup_data () {
    if [ -d "${TARGET_DIR}" ]; then
	    echo "Cleaning up"
	    echo "  Removing ${TARGET_DIR}"
	    rm -rf "${TARGET_DIR}"
	fi
}
