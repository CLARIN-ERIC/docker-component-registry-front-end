#!/bin/bash

REST_PROXY_CONF_FILE="//etc/nginx_compreg/http.d/compreg-rest-proxy.conf"
REST_PORT_WAIT_TIMEOUT="300"
REST_CHECK_INTERVAL="5"
REST_CHECK_PATH="rest/registry/profiles"

initialize() {
	#configure a REST proxy?
	if [ -n "${REST_SERVICE_PROXY_URL}" ]; then
		wait_for_rest
		enable_proxy
	elif [ -e ${REST_PROXY_CONF_FILE} ]; then
		disable_proxy
	else
		echo "REST service proxy not enabled"
	fi

}

wait_for_rest() {
	HOST_PORT="$(echo "${REST_SERVICE_PROXY_URL}" | sed -E 's_https?://([^/:]+:[0-9]+).*_\1_g')"
	echo "Checking/waiting for REST service at ${HOST_PORT} (extracted from ${REST_SERVICE_PROXY_URL}). Timeout: ${REST_PORT_WAIT_TIMEOUT}s"
	wait-for "${HOST_PORT}"  -t "${REST_PORT_WAIT_TIMEOUT}"
	
	echo "Checking/wating for REST response"
	while ! curl -lfs "${REST_SERVICE_PROXY_URL}/${REST_CHECK_PATH}" > /dev/null 2>&1; do
		echo "REST not available (yet). Exit code: $?"
		sleep "${REST_CHECK_INTERVAL}"
	done
	
	echo "Confirmed availability of REST service at ${REST_SERVICE_PROXY_URL}/${REST_CHECK_PATH}"
}

enable_proxy() {
	echo "Enabling REST service proxy (${REST_SERVICE_PROXY_URL})"
	cp "${REST_PROXY_CONF_FILE}.disabled" "${REST_PROXY_CONF_FILE}"
	replaceVarInFile "REST_SERVICE_PROXY_URL" "${REST_SERVICE_PROXY_URL}" "${REST_PROXY_CONF_FILE}"
}

disable_proxy() {
	echo "Removing configuration for REST service proxy (not enabled)"
	rm "${REST_PROXY_CONF_FILE}"
}

initialize
