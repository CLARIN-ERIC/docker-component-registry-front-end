#!/bin/bash
HTML_DIR="/var/www/component-registry-front-end"
ENABLED_FILE="maintenance.html"
DISABLED_FILE="maintenance-disabled.html"

is_enabled() {
  (cd "${HTML_DIR}" && [ -e "${ENABLED_FILE}" ] )
}

is_disabled() {
  (cd "${HTML_DIR}" && [ -e "${DISABLED_FILE}" ] )
}

enable() {
  echo "Enabling"
  (cd "${HTML_DIR}" && mv "${DISABLED_FILE}" "${ENABLED_FILE}")
}

disable() {
  echo "Disabling"
  (cd "${HTML_DIR}" && mv "${ENABLED_FILE}" "${DISABLED_FILE}")
}

main() {
  if [ "$#" -ne 1 ]; then
    echo "Usage: $0 [enable|disabled|status]"
    exit 1
  fi

  case "$1" in
    'enable')
      if ! is_enabled; then
        enable
      fi
      ;;
    'disable')
      if ! is_disabled; then
        disable
      fi
      ;;
    'status')
      if is_enabled; then echo 'Status: maintenance mode active'; fi
      if is_disabled; then echo 'Status: maintenance mode deactivated'; fi
      ;;
  esac
}

main "$@"
