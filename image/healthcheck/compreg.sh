#!/usr/bin/env bash
TEST_URL='http://localhost:80/ds/ComponentRegistry'
curl --silent --fail --location "${TEST_URL}" > /dev/null \
	|| (echo "Could not connect to ${TEST_URL}" && exit 1)
