gzip_min_length 10240;
gzip_types application/javascript text/javascript text/html application/x-javascript text/css text/plain application/xml text/xml application/json;
gzip_proxied any;
gzip_http_version 1.0;

location = / {
   return 301 " /ds/ComponentRegistry/";
}

location = /ds/ComponentRegistry {
   return 301 " /ds/ComponentRegistry/";
}

location ^~ /ds/ComponentRegistry/ {
  if (-f /var/www/component-registry-front-end/maintenance.html) {
     return 503;
  }

  alias   /var/www/component-registry-front-end/;
}

include /etc/nginx_compreg/http.d/compreg-rest-proxy*.conf;

location /maintenance-error {
  alias /var/www/component-registry-front-end;
}

location @maintenance {
  rewrite  ^(.*)$ /maintenance-error/maintenance.html last;
  break;
}

error_page   503  @maintenance;
error_page   500 502 504  /50x.html;
location = /50x.html {
	root    /var/lib/nginx/html;
}
